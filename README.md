<!-- README.md is generated from README.Rmd. Please edit that file -->

# massprocesser <img src="man/figures/massprocesser_logo.png" align="right" alt="" width="120" />

[![](https://www.r-pkg.org/badges/version/massprocesser?color=green)](https://cran.r-project.org/package=massprocesser)
[![](https://img.shields.io/github/languages/code-size/tidymass/massprocesser.svg)](https://github.com/tidymass/massprocesser)
[![Dependencies](https://tinyverse.netlify.com/badge/massprocesser)](https://cran.r-project.org/package=massprocesser)
[![](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)

`massprocesser` is a part of [tidymass](https://tidymass.github.io/tidymass/).

------

## **About**

`massprocesser` is a R package which is used for untargeted metabolomics data
processing and analysis.

-----

## **Installation**

You can install `massprocesser` from
[Github](https://github.com/tidymass/massprocesser).

``` r
# Install `massprocesser` from GitHub
if(!require(remotes)){
install.packages("remotes")
}
remotes::install_github("tidymass/massprocesser")
```

-----

## **Usage**

Please see the `Help document` page to get the instruction of `massprocesser`.

-----

## **Need help?**

If you have any quesitions about `massprocesser`, please don’t hesitate to
email me (<shenxt@stanford.edu>).

<i class="fa fa-weixin"></i>
[shenzutao1990](https://www.shenxt.info/files/wechat_QR.jpg)

<i class="fa fa-envelope"></i> <shenxt@stanford.edu>

<i class="fa fa-twitter"></i>
[Twitter](https://twitter.com/JasperShen1990)

<i class="fa fa-map-marker-alt"></i> [M339, Alway building, Cooper Lane,
Palo Alto,
CA 94304](https://www.google.com/maps/place/Alway+Building/@37.4322345,-122.1770883,17z/data=!3m1!4b1!4m5!3m4!1s0x808fa4d335c3be37:0x9057931f3b312c29!8m2!3d37.4322345!4d-122.1748996)

-----

## **Citation**

If you use `massprocesser` in you publication, please cite this publication:

X Shen, ZJ Zhu - Bioinformatics (Oxford, England), 2019, MetFlow: An
interactive and integrated workflow for metabolomics data cleaning and
differential metabolite discovery.  
[Web
Link](https://www.researchgate.net/profile/Xiaotao_Shen/publication/330410794_MetFlow_An_Interactive_and_Integrated_Workflow_for_Metabolomics_Data_Cleaning_and_Differential_Metabolite_Discovery/links/5cb3ca7892851c8d22ec3a89/MetFlow-An-Interactive-and-Integrated-Workflow-for-Metabolomics-Data-Cleaning-and-Differential-Metabolite-Discovery.pdf).

Thanks very much\!
